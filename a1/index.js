
	//create a hotel database
	//hotel

	//create collections
	//rooms


	//inserting single document in the rooms collection

	db.rooms.insertOne({
		name : "single",
		accomodates : 2,
		price : 1000,
		description : "A simple room with all the basic necessities",
		rooms_available : 10,
		isAvailable : false
	});

	
	//inserting multiple documents in the rooms collection

	db.rooms.insertMany([
		{
			name : "double",
			accomodates : 3,
			price : 2000,
			description : "A room fit for a small family going on a vacation",
			rooms_available : 5,
			isAvailable : false
		},
		{
			name : "triple",
			accomodates : 4,
			price : 2500,
			description : "A room fit for a small family or friends going on a vacation",
			rooms_available : 4,
			isAvailable : false
		}
	]);


	//find method

	db.rooms.find({name: "double"});


	//update method

	db.rooms.updateOne({name: "double"}, {$set: {name: "queen", rooms_available: 0}});


	//delete method

	db.rooms.deleteMany({rooms_available: 0});





